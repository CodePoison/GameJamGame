#include "raylib.h"
#include "GJ.hpp"
#include <vector>
#include <iostream>

int main(void)
{
    // Initialization
    //--------------------------------------------------------------------------------------
    const int screenWidth = 1200;
    const int screenHeight = 650;

    InitWindow(screenWidth, screenHeight, "Hell-runner");
    HideCursor();
    Vector2 Mouse{(0, 0)};
    //Vars Here
    bool Pause = false;
    int ReloadCoolDown = 40;
    //bullet stuff
    std::vector<Bullet> vecBullet;
    Vector3 BulletPos{(float)0, (float)0, (int)0};
    //x, y, z but y is gonna be used for rotation
    //NOTE: Raylib does not use radians
    bool Title = true;
    
    //player:
    Vector3 PlayerPos{(float)screenWidth/2, (float)screenHeight/2, (int)0}; 
    int ammo = 30; 
    int points = 500;
    bool IsDead = false;

    //Player Collisions
    bool Collision = true;
    //epic gun:
    int tick = 0;
    int tickinfps = 0;
    int lastshottick = 0;
    float firedelay = 5; //in ticks, every frame is 1 tick
    //dash
    int DashCoolDown = 30;
    int DashTime = 5;
    //timer
    int Time = 0;
    int Round = 0;
    bool RoundEnd = false;
    //card vector
    std::vector<Card> vecCard;
    std::vector<Card> vecCCard;
    //vector for open amount of cards (used as power up)

    //card stuff flags
    int Id = 0;
    bool Monster = false;
    int MonsterTimer = 120;
    //Shop
    bool Shop = false;
    int health = 50;
    int mags = 5;
    int damage = 1;
    float u1 = .5;
    float u2 = .5;
    float u3 = .5;
    float u4 = .5;
    Vector2 Health ({0, (int)screenHeight/2 - 64});
    Vector2 Mags ({256 * 1, screenHeight/2 - 64});
    Vector2 Damage ({256 * 2, screenHeight/2 - 64});
    Vector2 FireRate ({256 * 3, screenHeight/2 - 64});
    Vector2 next ({256 * 4 - 128, screenHeight/5 - 16});
    //Zombie stuff
    std::vector<Zombie> vecZombie;
    int ZombieDeadCount = 0;
    int FunNumber = rand() % 1;
    //------------


    SetTargetFPS(30);               // Set our game to run at 60 frames-per-second (yes)
    //--------------------------------------------------------------------------------------
    Texture2D AltTex = LoadTexture("res/Pause.png");
    Texture2D SpaceBar = LoadTexture("res/CardHow.png");
    Texture2D StartButton = LoadTexture("res/Start.png");
    Texture2D ControlsTex = LoadTexture("res/ControlsScreen.png");
    Texture2D BulletTex = LoadTexture("res/bullet.png");
    Texture2D CardTex = LoadTexture("res/card.png");
    Texture2D PlayerTex = LoadTexture("res/player.png");
    Texture2D MonsterTex = LoadTexture("res/Monster.png");
    Texture2D ZombieTex = LoadTexture("res/zombie.png");
    Texture2D PointTex = LoadTexture("res/Pointer.png");
    Texture2D NormalBG = LoadTexture("res/NormalBG.png");
    Texture2D MonsterBG = LoadTexture("res/CardBg.png");
    Texture2D AmmoCardTex = LoadTexture("res/ammocard.png");
    Texture2D HealthCardTex = LoadTexture("res/healthcard.png");
    Texture2D DamageCardTex = LoadTexture("res/damagecard.png");
    Texture2D FireRateTex = LoadTexture("res/firerate.png");
    Texture2D NextTex = LoadTexture("res/ready.png");

    // sound yes great

    InitAudioDevice();

    Sound zombiekill = LoadSound("res/hurt2.wav");
    Sound shoot = LoadSound("res/shoot.wav");

    // Main game loop 
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {  
        tick++;
        if(tick % 30 == 0 && !Title && !Pause)
            Time += 1; 

        if (PlayerPos.x <= 0 ) PlayerPos.x = 0;
        if (PlayerPos.x >= screenWidth ) PlayerPos.x = screenWidth;
        if (PlayerPos.y >= screenHeight) PlayerPos.y = screenHeight;
        if (PlayerPos.y <=  0) PlayerPos.y = 0;
        
        

        if (vecBullet.size() >= 32)
            vecBullet.erase(vecBullet.begin());
        if (vecCard.size() > 3)
            vecCard.erase(vecCard.begin());

        float Delta = GetFrameTime();
        (int) BulletPos.z % 360;// z is used for rotation so i make sure i doesn't do over 360
        //Colision before input
        int MouseX = GetMouseX();
        int MouseY = GetMouseY();
        Mouse.x = MouseX;
        Mouse.y = MouseY;
        PlayerPos.z = GetAngle(MouseY, PlayerPos.y, MouseX, PlayerPos.x);

        if (Title)
            if (IsMouseButtonPressed(0))
                if (CheckCollisionPointRec(Vector2{Mouse.x, Mouse.y}, Rectangle{ screenWidth/2 - 182, screenHeight/2 - 32, (float) StartButton.width,(float) StartButton.height}))
                    Title = false;
        
        if (IsKeyPressed(KEY_RIGHT_ALT) || IsKeyDown(KEY_LEFT_ALT))
        {
            if (IsDead == false)
            {
                switch(Pause) 
                {
                    case false:
                        Pause = true;
                    break;

                    case true:
                    Pause = false;
                    break;
                }
            }
            switch(IsDead)
            {
                case true:
                    u1 = 0;
                    u2 = 0;
                    u3 = 0;
                    u4 = 0;
                    health = 50;
                    Round = 0;
                    mags = 5;
                    damage = 1;
                    points = 500;
                    IsDead = false;
                    Time = 0;
                    ZombieDeadCount = 0;
                    vecCard.empty();
                    vecCCard.empty();
                break;

            }  
                
        } 
        if (Pause == false)
        {
            if (IsKeyDown(KEY_RIGHT)|| IsKeyDown(KEY_D)) PlayerPos.x += 15.0f; //gonna have rotation here instead of movement
            if (IsKeyDown(KEY_LEFT)|| IsKeyDown(KEY_A))  PlayerPos.x -= 15.0f; //ignore this or add mouse rotations
            if (IsKeyDown(KEY_UP)|| IsKeyDown(KEY_W))  PlayerPos.y -= 15.0f;
            if (IsKeyDown(KEY_DOWN)|| IsKeyDown(KEY_S)) PlayerPos.y += 15.0f;
            if (IsKeyDown(KEY_ONE)){ points += 500; health += 40;}  //hacks
            if (IsKeyPressed(KEY_SPACE))
            {
                if (vecCCard.size() > 0)
                {
                    vecCCard.pop_back();
                    Monster = true;
                }
            }
            if (Monster == true)
            {
                Collision = false;
                MonsterTimer--;
                if (MonsterTimer <= 0)
                {
                    Monster = false;
                    MonsterTimer = 120;
                }
            }
            
            if (IsMouseButtonDown(1) && DashTime > 0 )
            {
                //Dash
                PlayerPos.x = PlayerPos.x + (cos(PlayerPos.z * PI / 180)* -120.00f );
                PlayerPos.y = PlayerPos.y + (sin(PlayerPos.z * PI / 180)* -120.00f );
                Collision = false;
                DashTime--;
            }
            Collision = true;
            if(DashTime <= 0)
            {
                //have a reload timer
                DashCoolDown--;
                DashTime = 0;
                if (DashCoolDown == 0)
                {
                    DashTime = 5;
                    DashCoolDown = 30;
                }
            }
            
            if (IsMouseButtonDown(0))
            {
                if (Shop == false && IsDead == false && Title == false) {
                    if (tick > lastshottick + firedelay && mags > 0) {
                        ammo--;
                        if (ReloadCoolDown == 40) {
                            lastshottick = tick;
                            vecBullet.push_back({PlayerPos.x, PlayerPos.y, (int) PlayerPos.z + 180});
                            PlaySound(shoot);
                        }
                    }
                }
            }
            //Zombie Spawn --------------------------------------------
            if (tick % 40 - (Round * 3)== 0 && IsDead == false && RoundEnd == false && !Title)
                vecZombie.push_back({(float)GetRandomValue(0, 120), (float)GetRandomValue(0, screenHeight), 0, 9 + (Round * 2)});
            if (tick % 45 - (Round * 3) == 0 && IsDead == false && RoundEnd == false && !Title)
                vecZombie.push_back({(int)screenWidth + (float) GetRandomValue(0, 120),(float)GetRandomValue(0, screenHeight), 0, 9 + (Round * 2)});
            if (Time == 30)
                RoundEnd = true;

            if (ZombieDeadCount == FunNumber)
            {
                vecCard.push_back({(float)GetRandomValue(0, screenWidth),(float) GetRandomValue(0, screenHeight), Id += 1});
                FunNumber = 0; FunNumber = rand() % 50 ;// reset counter till next card
                ZombieDeadCount = 0;
            }
            if (RoundEnd == true && vecZombie.size() == 0)
            {
                Shop = true;
                Time = 0;
            }
            //---------------------------------------------------------
            if(ammo <= 0)
            {
                //have a reload timer
                ReloadCoolDown--;
                ammo = 0;
                
                if (ReloadCoolDown == 0)
                {
                    ammo = 30;
                    ReloadCoolDown = 40;
                    mags--;
                }
            }
            //SHOP ------------------------------------------------------
            if (Shop == true)
            {
                ShowCursor();
                //Damage Card
                if (IsMouseButtonPressed(0) && CheckCollisionPointRec(Vector2{Mouse.x, Mouse.y}, Rectangle{Damage.x, Damage.y,(float) DamageCardTex.width,(float) DamageCardTex.height}))
                    if(points > 500 * (u1 / 2))
                    {
                        points -= 500 * (u1 / 2);
                        damage += 2;
                        u1++;
                    }
                //Health Card
                if (IsMouseButtonPressed(0) && CheckCollisionPointRec(Vector2{Mouse.x, Mouse.y}, Rectangle{Health.x, Health.y,(float) DamageCardTex.width,(float)  DamageCardTex.height}))
                    if(points > 500 * (u2 / 2))
                    {
                        points -= 500 * (u2 / 2);
                        health += 30;
                        u2++;
                    }
                //Ammo card
                if (IsMouseButtonPressed(0) && CheckCollisionPointRec(Vector2{Mouse.x, Mouse.y}, Rectangle{Mags.x, Mags.y,(float)  DamageCardTex.width,(float)  DamageCardTex.height}))
                    if(points > 500 * (u3 / 2))
                    {
                        points -= 500 * (u3 / 2);
                        mags += 5;
                        u3++;
                    }    
                //FireRate
                if (IsMouseButtonPressed(0) && CheckCollisionPointRec(Vector2{Mouse.x, Mouse.y}, Rectangle{FireRate.x, FireRate.y,(float)  DamageCardTex.width,(float)  DamageCardTex.height}))
                    if(points > 500 * (u4 / 2))
                    {
                        points -= 500 * (u4 / 2);
                        firedelay -= 0.2f;
                        u4++;
                    }    
                //Next
                if (IsMouseButtonPressed(0) && CheckCollisionPointRec(Vector2{Mouse.x, Mouse.y}, Rectangle{next.x, next.y,(float) NextTex.width,(float) NextTex.height}))
                {
                    Round++;
                    Shop = false;
                    RoundEnd = false;
                    HideCursor();
                }
            }
            // ----------------------------------------------------------
            for (auto &c : vecCard)
            {   
                if (CheckCollisionRecs(Rectangle{c.x, c.y,(float)CardTex.height/5,(float)CardTex.height/5}, Rectangle{PlayerPos.x, PlayerPos.y,(float)PlayerTex.height,(float)PlayerTex.width}))
                {
                    vecCCard.push_back({0, 0, Id +=1});
                    vecCard.erase(vecCard.begin());
                    c.Id -= 1;
                }
                for(auto &a : vecCCard)
                    if( vecCCard.size() > 4)
                    {
                        vecCCard.erase(vecCCard.begin());
                        c.Id -= 1;
                    }

            }
            for (auto &b : vecBullet)
            {   
                //look through all bullets can change their position
                b.x = b.x + (cos(b.rot * PI / 180)* 40.00f );
                b.y = b.y + (sin(b.rot * PI / 180)* 40.00f );
                //move bullet forward based on rotation
                
            }
            
            for (auto &b : vecZombie)
            {   
                b.rot = GetAngle(b.y, PlayerPos.y, b.x, PlayerPos.x);
                //look through all bullets can change their position
                b.x = b.x + (cos(b.rot * PI / 180)* 10.00f + (2 * Round));
                b.y = b.y + (sin(b.rot * PI / 180)* 10.00f + (2 * Round));
                if (b.health < 1 )
                {
                    PlaySound(zombiekill);
                    vecZombie.erase(vecZombie.begin());
                    points += 50;
                    ZombieDeadCount++;
                }
                for (auto &z : vecBullet)
                    if (CheckCollisionRecs(Rectangle{z.x, z.y,(float)BulletTex.width/5,(float)BulletTex.height/5}, Rectangle{b.x,b.y,256,256})) 
                        {  
                            b.health -= damage;
                        }
                if (Collision == true && CheckCollisionRecs(Rectangle{PlayerPos.x, PlayerPos.y,(float)PlayerTex.width/2,(float) PlayerTex.height/2}, Rectangle{b.x,b.y,(float)ZombieTex.width/2,(float)ZombieTex.height/2}))
                {
                    if (Monster)
                        
                        vecZombie.erase(vecZombie.begin());  
                    if (!Monster)  
                        health -= 1 + (1 * Round);
                } 
            }
        }
        if (health <= 0)
        {
            IsDead = true;
            vecZombie.clear();
        }
        
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();

        ClearBackground(GRAY);
        DrawTextureSuper(MonsterBG, 0, 0, 0, 5.6, WHITE);
        if (!Monster)
            DrawTextureSuper(NormalBG, 0, 0, 0, 4.20, WHITE);
        
            

        //DRAW ACTUAL SPRITES HERE
        
        if (Title)
        {
            DrawText("Gun'em up in hell", screenWidth/2 - 192, screenHeight/2 - 64, 64, WHITE);
            DrawTexture(StartButton, screenWidth/2 - 182, screenHeight/2 - 32, WHITE);
            DrawTextureSuper(AltTex, 10 , screenHeight - AltTex.height/2 - 20, 0, 0.50, WHITE);
            DrawTexture(SpaceBar, screenWidth/2 - ControlsTex.width - SpaceBar.width, screenHeight - SpaceBar.height, WHITE);
            DrawTextureSuper(ControlsTex, screenWidth - ControlsTex.width, screenHeight - ControlsTex.height, 0, 1, WHITE);
        }
        else if (Shop)
        {
            DrawText(TextFormat("Mags left: %02i", mags), 0, 0, 48, BLACK);
            DrawText(TextFormat("Health: %02i", health), 0, 48, 48, RED);
            DrawText(TextFormat("Points: %02i", points), 0, 48 * 2, 48, LIME);
            DrawText(TextFormat("Damage: %02i", damage), 0, 48 * 3, 48, WHITE);
            DrawTextureSuper(DamageCardTex, Damage.x, Damage.y, 0, 1, WHITE);
            DrawText(TextFormat("Cost: %02.0f ", 500 * (u1 / 2)), Damage.x, Damage.y + DamageCardTex.height, 32, WHITE);
            DrawTextureSuper(HealthCardTex, Health.x, Health.y, 0, 1, WHITE);
            DrawText(TextFormat("Cost: %02.0f ", 500 * (u2 / 2)), Health.x, Health.y + HealthCardTex.height, 32, WHITE);
            DrawTextureSuper(AmmoCardTex, Mags.x, Mags.y, 0, 1, WHITE);
            DrawText(TextFormat("Cost: %02.0f ", 500 * (u3 / 2)), Mags.x, Mags.y + AmmoCardTex.height, 32, WHITE);
            DrawTextureSuper(FireRateTex, FireRate.x, FireRate.y, 0, 1, WHITE);
            DrawText(TextFormat("Cost: %02.0f ", 500 * (u4 / 2)), FireRate.x, FireRate.y + FireRateTex.height, 32, WHITE);
            DrawTextureSuper(NextTex, next.x, next.y, 0, 1, WHITE);
        }           
        else if(IsDead == true)
        {    
            DrawText("you died",screenWidth/2 - 192, screenHeight/2 - 64, 64, WHITE );
            DrawText("Press alt to restart",screenWidth/2 - 192, screenHeight/2 , 16, WHITE);
        }
        else if(!Title)
        {
            if (Pause == true)
            {
                DrawText("Paused",screenWidth/2 - 192, screenHeight/2 - 64, 32, WHITE );
            }
            
            DrawText(TextFormat("Health: %02i", health), screenWidth - 288, 0, 48, WHITE);

            DrawText(TextFormat("Mags: %02i", mags), screenWidth - 288, 48, 48, WHITE);

            if (Monster)
            {
                DrawText("Monster Mode", screenWidth/2 - 257, 256, 48, LIME);
                DrawTexturePro(MonsterTex, Rectangle{ 0, 0,(float)PlayerTex.width,(float)PlayerTex.height }, Rectangle{ PlayerPos.x, PlayerPos.y,(float)PlayerTex.width,(float)PlayerTex.height }, Vector2{ PlayerTex.width * 0.5f, PlayerTex.height * 0.5f  }, PlayerPos.z, WHITE);
            }
            if (!Monster)
                DrawTexturePro(PlayerTex, Rectangle{ 0, 0,(float)PlayerTex.width,(float)PlayerTex.height }, Rectangle{ PlayerPos.x, PlayerPos.y,(float)PlayerTex.width,(float)PlayerTex.height }, Vector2{ PlayerTex.width * 0.5f, PlayerTex.height * 0.5f  }, PlayerPos.z, WHITE);

            
            for (auto &c : vecZombie)
                DrawTexturePro(ZombieTex, Rectangle{ 0, 0,(float)PlayerTex.width,(float)PlayerTex.height }, Rectangle{ c.x, c.y,(float)PlayerTex.width,(float)PlayerTex.height }, Vector2{ PlayerTex.width * 0.5f, PlayerTex.height * 0.5f  }, c.rot - 180, WHITE);

            for (auto &b : vecBullet)                                                                                                                                                                                    
                DrawTextureSuper(BulletTex, b.x + cos(b.rot * PI / 180) * 95, b.y + sin(b.rot * PI / 180) * 100, b.rot + 90, 0.20, WHITE);
            //this is setup to have any amount of bullets
            

            DrawRectangle(screenWidth/2 - 164, screenHeight - 64, 256, 128, GRAY);
            for (auto &c : vecCard)
            {   
                DrawTextureSuper(CardTex, c.x, c.y, 0, 0.20, WHITE);
            }
            for (auto &c : vecCCard)
            {   
                DrawTextureSuper(CardTex, screenWidth/2 - 158 + ( 10 * c.Id), screenHeight - 64, 0, 0.25, WHITE);
            }

            DrawText(TextFormat("Ammo: %02i", ammo), 0, 0, 48, RED);
        
            if (ammo == 0)
                DrawText("Reloading", PlayerPos.x, PlayerPos.y, 32, RED);
            
            DrawText(TextFormat("Points: %02i",points), 0, 48, 48, LIME);
        }
        if (!Shop)
            DrawTextureSuper(PointTex, Mouse.x, Mouse.y - PointTex.height/5 - 20, 0, 0.2, WHITE);
        //----------------------------------------------------------------------------------
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    UnloadTexture(AltTex);
    UnloadTexture(SpaceBar);
    UnloadTexture(StartButton);
    UnloadTexture(ControlsTex);
    UnloadTexture(MonsterTex);
    UnloadTexture(MonsterBG);
    UnloadTexture(NextTex);
    UnloadTexture(AmmoCardTex);
    UnloadTexture(DamageCardTex);
    UnloadTexture(HealthCardTex);
    UnloadTexture(NormalBG);
    UnloadTexture(PointTex);
    UnloadTexture(CardTex);
    UnloadTexture(BulletTex);
    UnloadTexture(PlayerTex);
    UnloadTexture(ZombieTex);

    UnloadSound(shoot);
    UnloadSound(zombiekill);

    CloseAudioDevice();

    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}// note if you look closely the bullets can curve lol also hi if your looking through the source after the jam